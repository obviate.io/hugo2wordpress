import yaml # pip3 install pyyaml
import markdown # pip3 install markdown
import re
import jinja2 # pip3 install jinja2
import sys
from pprint import pprint
import requests
import json
import os
import time

#### Globals vars to update ####
authCreds = ('UserNameGoesHere', 'PasswordGoesHere')
wpBaseURL = 'https://mywebsite.tld/wp-json/wp/v2/'
imgBaseURL = 'https://cdn.mywebsite.tld/'
postDir = 'content/posts'
#################################
allCat = {}
allTag = {}
allMedia = {}
allUsers = {}
headers = {'Accept': 'application/json' }
BASE_DIR = os.path.dirname(os.path.realpath(__file__))

def tImg(optIn):
    # Translate the various possible image templates into a single output format
    # Imperfect wordpress translation as it doesn't treat these responsive like
    # new image blocks. Unless you go in and update the article
    out = re.findall('(\w*)="(.*?)"', optIn)
    opts = {}
    for xx in out:
        opts[xx[0]] = xx[1]

    url = opts['src'].strip("/")
    caption = ""
    try:
        caption = opts['c']
    except:
        pass
    try:
        caption = opts['caption']
    except:
        pass
    try:
        caption = opts['alt']
    except:
        pass
    str = ''
    str += f'<figure class="wp-block-image"><img src="{imgBaseURL}{url}" alt="{caption}" width="600px"/>'
    if len(caption) > 1:
        str += f'<figcaption class="wp-element-caption">{caption}</figcaption></figure>'
    else:
        str += f'</figure>'
    return(str)

def tTweets(opts):
    # Translate Twitter template to embed
    oo = opts.split(" ")
    str = (
        '\n<!-- wp:embed {"url":"'
        f'https://twitter.com/{oo[0]}/status/{oo[1]}'
        '","type":"rich","providerNameSlug":"twitter","responsive":true} -->\n'
        '<figure class="wp-block-embed is-type-rich is-provider-twitter wp-block-embed-twitter"><div class="wp-block-embed__wrapper">\n'
        f'https://twitter.com/{oo[0]}/status/{oo[1]}\n'
        '</div></figure>\n'
        '<!-- /wp:embed -->\n\n'
    )
    return(str)

def tYoutube(opts):
    # Translate Youtube template to embed
    opts = opts.strip()
    str = (
        '\n\n<!-- wp:embed {"url":"https://www.youtube.com/watch?v='
        f'{opts}'
        '","type":"video","providerNameSlug":"youtube","responsive":true,"className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->\n'
        '<figure class="wp-block-embed is-type-video is-provider-youtube wp-block-embed-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio"><div class="wp-block-embed__wrapper">\n'
        f'https://www.youtube.com/watch?v={opts}\n'
        '</div></figure>\n'
        '<!-- /wp:embed -->\n\n'
    )
    return(str)

def tIcon(opts):
    # Drop icon template
    str = ''
    return(str)

def tHighlight(opts):
    # Tranlsate 
    if opts == "/":
        str = '</code></pre>'
    else:
        str = '<pre class="wp-block-code"><code>'
    return(str)

def getAllCat():
    # Get all the categories from wordpress and store name/ID locally
    page = 1
    while page > 0:
        res = requests.get(wpBaseURL + f'categories?per_page=100&page={page}', headers=headers, auth=authCreds)
        out = res.json()
        if len(out) > 0:
            for cat in out:
                allCat[cat['name'].casefold()] = cat['id']
            page = page + 1
        else:
            return True

def getCat(n):
    # Get a specific categories ID, or create it if it doesn't exist
    if n.casefold() in allCat:
        return allCat[n.casefold()]
    else:
        res = requests.post(wpBaseURL + 'categories', headers=headers, json={"name":n}, auth=authCreds)
        #pprint(res.json())
        newId = res.json()['id']
        allCat[n.casefold()] = newId
        return newId

def getAllTag():
    # Get all the tags from wordpress and store name/ID locally
    page = 1
    while page > 0:
        res = requests.get(wpBaseURL + f'tags?per_page=100&page={page}', headers=headers, auth=authCreds)
        out = res.json()
        if len(out) > 0:
            for tag in out:
                allTag[tag['name'].casefold()] = tag['id']
            page = page + 1
        else:
            return True

def getTag(n):
    # Get a specific tags ID, or create it if it doesn't exist
    if n.casefold() in allTag:
        return allTag[n.casefold()]
    else:
        res = requests.post(wpBaseURL + 'tags', headers=headers, json={"name":n}, auth=authCreds)
        newId = res.json()['id']
        allTag[n.casefold()] = newId
        return newId

def getAllMedia():
    # Get a list of all media names and IDs to be stored locally
    page = 1
    while page > 0:
        res = requests.get(wpBaseURL + f'media?per_page=100&page={page}', headers=headers, auth=authCreds)
        out = res.json()
        if len(out) > 0 and res.status_code != 400:
            for med in out:
                if 'original_image' in med['media_details']:
                    allMedia[med['media_details']['original_image'].casefold()] = med['id']
                else:
                    allMedia[med['media_details']['sizes']['full']['file'].casefold()] = med['id']
            page = page + 1
        else:
            return True

def getFile(fName):
    # Get the contents of a single markdown file, split the header from the content

    f = open(fName,"rt")
    all = f.read()
    x = re.split("---\n",all)
    rawContent = x[2]
    header = yaml.safe_load(x[1])
    return (rawContent, header)

def findFixAndRender(rawContent):
    # Find all template tags, get them parsed into HTML
    # then finally do some other minor fixes and render everything to HTML

    xx = re.findall("{{<(.*?)>}}",rawContent)
    fixer = []
    for a in xx:
        find = "{{<" + a + ">}}"
        out = re.search("^(\w*) (.*)", a.strip())
        if out is not None:
            key = out[1]
            opts = out[2]
        elif a.strip() == "/ highlight":
            key = "highlight"
            opts = "/"
        else:
            print(f"Line ingest issue: ||{a}||")
            sys.exit(1)

        if key == "img":
            # src="2021/flowers.jpg" s="left"
            # img src="2020/tweet-1275381046448730114.png" c="From Archive.org - Original tweet deleted"
            replace = tImg(opts)
        elif key == "imgleft":
            # src="/wp-content/uploads/ferrarimb-ms.jpg" alt=""
            replace = tImg(opts)
        elif key == "imgright":
            # caption="Elon Musk is a funny and brave man."
            replace = tImg(opts)
        elif key == "tweet":
            # tweet MarkVillacampa 1275200446764912643
            replace = tTweets(opts)
        elif key == "youtube":
            # youtube gDOUvOTX-gA
            replace = tYoutube(opts)
        elif key == "icon":
            # icon class="fab fa-gitlab"
            replace = tIcon(opts)
        elif key == "highlight":
            replace = tHighlight(opts)
        else:
            # If the template key isn't handled above, throw a note and move on.
            print(f"Code not found: |{key}|")
        fixer.append([find,replace])
    
    for repp in fixer:
        # Take all the previous translation work and actually apply it to our raw content
        rawContent = rawContent.replace(repp[0],repp[1])

    # Some bad content encoding from who knows how long ago. Quick fix.
    srPair = {"â€™": "'", "â€œ": '"', "â€": '"'}
    for key, val in srPair.items():
        while key in rawContent:
            rawContent = rawContent.replace(key, val)

    # Returning the completed content rendered from markdown to HTML
    return markdown.markdown(rawContent)

def featuredImage(header):
    # If the header defines a featured image, look for it in the media already uploaded to site
    # and use that existing ID, or upload the file and log the ID.

    if 'featured_image' in header:
        fullPath = os.path.join(BASE_DIR, "content/images", header['featured_image'])
        if os.path.exists(fullPath):
            fName = os.path.basename(fullPath)
            if fName.casefold() in allMedia:
                # Already have the image uploaded
                return allMedia[fName]
            else:
                # Image needs to be uploaded
                file_data = open(fullPath, 'rb').read()
                res = requests.post(wpBaseURL + 'media', headers={'Accept': 'application/json', 'Content-Disposition': 'attachment; filename='+fName}, auth=authCreds, data=file_data)
                out = res.json()
                if 'original_image' in out['media_details']:
                    allMedia[out['media_details']['original_image'].casefold()] = out['id']
                else:
                    allMedia[out['media_details']['sizes']['full']['file'].casefold()] = out['id']
                return out['id']
        else:
            # File does not exist on file system.
            print(f"File Does Not Exist: {fullPath} -- From: {header['title']}")    
            return False
    # No Featured Image specified
    return False
        
def getTargetList():
    # Get a list of all markdown files (aka targets, aka blog posts)
    allFiles = []
    fullPath = os.path.join(BASE_DIR, postDir)
    for root, dirs, files in os.walk(fullPath):
        
        for file in files:
            if file.endswith('.md'):
                f = os.path.join(BASE_DIR,root,file)
                allFiles.append(f)
    return sorted(allFiles)

def getAllUsers():
    # Get all the users from wordpress. Note that their "Name" must be identical
    # to what it is in the markdown headers for a match to be found.
    res = requests.get(wpBaseURL + f'users?per_page=100', headers=headers, auth=authCreds)
    for usr in res.json():
        allUsers[usr['name']] = usr['id']

def main():
    # Start by getting all the existing data needed from wordpress
    # and our list of files to proccess
    getAllUsers()
    getAllCat()
    getAllTag()
    getAllMedia()
    allFiles = getTargetList()
    
    count = 0
    for tgt in allFiles:
        # Proccess each file
        print(f"Proccessing {os.path.basename(tgt)}")
        (rawContent, header) = getFile(tgt)
        if 'draft' not in header:
            # This is basically "how to resume transfer"
            # Get existing slug from wordpress and compare against current file
            # If the slug matches, we assume it's been uploaded in a previous run.
            slug = header['url'].strip("/").split("/")[-1]
            res1 = requests.get(wpBaseURL + f'posts/?slug={slug}', headers=headers, auth=authCreds)
            out1 = res1.json()
            exists = False
            for a in out1:
                if a['slug'] == slug:
                    exists = True
            if exists:
                print("*** SKIPPING - Probably Already Exists")
            else:
                # Get ready to ACTUALLY submit a new post to wordpress.
                renderedContent = findFixAndRender(rawContent)

                # Translate string categories to int categories
                outCat = []
                if 'categories' in header:
                    for cat in header['categories']:
                        outCat.append(getCat(str(cat)))

                # Translate string tags to int tags
                outTag = []
                if 'tags' in header:
                    for tag in header['tags']:
                        outTag.append(getTag(str(tag)))

                # Define most of the post meta-data & content
                json_data = {
                    'title': header['title'],
                    'date': header['date'].strftime("%Y-%m-%dT08:00:00"), #2022-11-20T17:14:16
                    'status': 'publish',
                    'author': allUsers[header['author']],
                    'type': 'page',
                    'categories': outCat,
                    'tags': outTag,
                    'slug': slug, #Slug is only post-date portion.
                    'content': renderedContent,
                }

                # Do we need to add a featured image to this post?
                fImgID = featuredImage(header)
                if fImgID:
                    json_data['featured_media'] = fImgID


                # Submit post to wordpress!
                res = requests.post(wpBaseURL + 'posts', headers=headers, json=json_data, auth=authCreds)

                # Check that this was successful
                if res.status_code == 201:
                    print(f"Upload success: {res.json()['link']}")
                else:
                    print(f"FAILURE: {res.status_code}")

                # If the response is slow, that's a warning we might have overloaded wordpress
                # Take a nap and let PHP/Database garbage collection run.
                if res.elapsed.total_seconds() > 5:
                    print(f"Slow Request: {res.elapsed.total_seconds()} - Sleeping")
                    time.sleep(60)

                # Every 20 posts, take a nap so as to not overload the server.
                count = count + 1
                if count >= 20:
                    print("20 posts, sleeping")
                    time.sleep(30)
                    count = 0

    print("Done")

if __name__ == "__main__":
    main()