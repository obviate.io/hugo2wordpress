# hugo2wordpress
This is a script I wrote for the proccess of converting the https://obviate.io blog from Hugo Static Site (aka Markdown) to Wordpress. The script was written with my single usecase in mind, so it's not something you can simply pull off the shelf and use directly - however with a small bit of python skill anyone should be able to use it for their own needs.

The script was designed to run in the root directory of a hugo site repository. As such this repo also includes some sample images and posts in the normal hugo directory layout.

Note: You must install the [WordPress REST API Authentication by miniOrange](https://wordpress.org/plugins/wp-rest-api-authentication/) and enable basic auth. Or adjust the script to use another auth mechanism. Once the migration is completed you can disable/remove the plugin.

Please see https://obviate.io/?p=1335 for a more detailed writeup.

## License
MIT - See [LICENSE]
