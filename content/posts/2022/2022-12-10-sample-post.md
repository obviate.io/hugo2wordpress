---
title: "Sample Post Title"
date: 2022-12-10
author: MyName
featured_image: 2022/sample1.png
categories:
  - Category1
tags:
  - Tag1
  - TAG2
  - EXAMPLE TAG
  - AnotherCoolTag
type: post
url: "/2022/12/10/sample-post-title/"
---
Bacon ipsum dolor amet drumstick landjaeger meatloaf chicken shank. Jowl t-bone pork belly kevin chicken, landjaeger rump flank leberkas. Jowl cupim strip steak, t-bone kielbasa sirloin turducken prosciutto tongue bacon chuck doner jerky sausage. Biltong doner jerky jowl turducken.

<!--more-->

{{<img src="2022/sample2.png" s="right" c="This is a caption">}} Lorem ipsum dolor sit amet, [consectetur adipiscing elit](https://obviate.io/). Aliquam non orci rutrum, tristique metus eget, porttitor sapien. Donec in consectetur sapien, id cursus ipsum. Proin congue enim dolor, ultricies mattis neque aliquet quis. In hac habitasse platea dictumst. Sed eget commodo metus, ac iaculis enim. Maecenas dignissim porta nibh. Aenean turpis nisi, tincidunt ut augue sit amet, eleifend aliquam lacus. Cras id mauris a ex commodo facilisis sit amet vel erat. Aliquam at elit augue. Sed eget ultricies diam, et rutrum metus. Etiam at pharetra metus. Integer [molestie lobortis erat](https://obviate.io), non interdum turpis. Proin varius mauris vel imperdiet pulvinar. Aenean scelerisque placerat interdum. Suspendisse vel lacus nec mi tincidunt rhoncus sit amet eu purus.

{{< tweet MarkVillacampa 1275515766415204358 >}}

{{<imgleft src="2022/sample3.png">}} Future-proof land the plane. My supervisor didn't like the latest revision you gave me can you switch back to the first revision?. Lose client to 10:00 meeting organic growth high-level run it up the flag pole five-year strategic plan lose client to 10:00 meeting, yet slipstream. Tiger team iâ€™ve been doing some research this morning and we need to better organic growth. Optics you better eat a reality sandwich before you walk back in that boardroom, so killing it, or if you're not hurting you're not winning nail it down. I'll book a meeting so we can solution this before the sprint is over prethink value prop. Put it on the parking lot low engagement this is not the hill i want to die on. Level the playing field.

{{< youtube gDOUvOTX-gA >}}

The need to create a new [{{<icon class="fab fa-gitlab">}}taxonomy](https://gitlab.com/obviate.io/hugo2wordpress) that isn't just applying to our own solar system will become so evident and apparent that something will come out of it. I'm sure of it, even if it's not tomorrow. I claim that all those who think they can cherry-pick science simply don't understand how science works. That's what I claim. And if they did, they'd be less prone to just assert that somehow scientists are clueless.

{{<img src="2022/sample4.png" s="right">}} You do? Because, you might regret it later in life. My god, it's my mother. Put your pants back on. Wait a minute, what are you doing, Doc? No, Marty, we've already agreed that having information about the future could be extremely dangerous. Even if your intentions are good, they could backfire drastically. Whatever you've got to tell me I'll find out through the natural course of time.

{{< highlight javascript "linenos=table,linenostart=118">}}
color.on('color', function(msg){
 console.log(msg);
 document.body.style.backgroundColor = msg;
})
{{< / highlight >}}

In the end, that's what this election is about. I know, too, that Islam has always been a part of America's story. When violent extremists operate in one stretch of mountains, people are endangered across an ocean. For human history has often been a record of nations and tribes subjugating one another to serve their own interests. Make no mistake: we do not want to keep our troops in Afghanistan. The Talmud tells us: "The whole of the Torah is for the purpose of promoting peace."